<?php 
function wp_crawler_setting_page() {

    $array_post_time = array( 1, 2, 5, 10, 20, 30, 60 );
    $array_task_time = array(5, 10, 20, 30, 60, 120, 360, 720, 1440);
    $array_task_count = array(1, 2, 3, 4, 5, 10);
    $array_post_count = array(1, 2, 3, 5, 8, 10, 12, 15, 20);
    $alert = null;
    if(isset($_POST['btnSubmit'])) {
        try {
            $task_schedule_time = isset($_POST['task-schedule-time']) ? ((int) $_POST['task-schedule-time'] * 60) : 300;
            $post_schedule_time = isset($_POST['post-schedule-time']) ? ((int) $_POST['post-schedule-time'] * 60) : 60;
            $task_schedule_count= isset($_POST['task-schedule-count']) ? ((int) $_POST['task-schedule-count']) : 1;
            $post_schedule_count = isset($_POST['post-schedule-count']) ? ((int) $_POST['post-schedule-count']) : 1;
            if (get_option('task-schedule-time'))
                update_option('task-schedule-time',$task_schedule_time);
            else 
                add_option('task-schedule-time',$task_schedule_time);

            if (get_option('post-schedule-time'))
                update_option('post-schedule-time',$post_schedule_time);
            else 
                add_option('post-schedule-time',$post_schedule_time);

            if (get_option('task-schedule-count'))
                update_option('task-schedule-count',$task_schedule_count);
            else 
                add_option('task-schedule-count',$task_schedule_count);

            if (get_option('post-schedule-count'))
                update_option('post-schedule-count',$post_schedule_count);
            else 
                add_option('post-schedule-count',$post_schedule_count);

            $alert = 'success';
        } catch (Exception $e) {
            $alert = $e;
        }

    }
    
    ?>
    <div class="row">
        <div class="col-lg-11">
            <h1 class="page-header" style="margin-bottom:0px">Cài đặt</h1>
        </div>
    </div>
    <div class="row" style="margin-bottom:5px">
        <div class="col-lg-11">
        <?php if ($alert == 'success') { ?>

            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Đã cập nhật thành công</strong>
            </div>
        <?php } else if ($alert != false) { ?>
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $alert; ?></strong>
            </div>
        <?php } ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <form method="POST" action="" >
        
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cài đặt chung
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">

                    <div class="form-group">
                        <label for="preview-title-post">Chọn thời gian chạy chiến dịch tự động</label>
                        <select id="task-schedule-time" name="task-schedule-time" class="form-control">
                            <?php
                            foreach($array_task_time as $time) {
                                if(get_option('task-schedule-time') && get_option('task-schedule-time') == $time*60) {
                                    echo '<option selected value="'.$time.'">'.$time.' phút</option>';
                                } else {
                                    echo '<option value="'.$time.'">'.$time.' phút</option>';
                                }
                            }
                            ?>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="preview-title-post">Chọn số chiến dịch trong 1 lần thu thập</label>
                        <select id="task-schedule-count" name="task-schedule-count" class="form-control">
                            <?php
                            foreach($array_task_count as $item) {
                                if(get_option('task-schedule-count') && get_option('task-schedule-count') == $item) {
                                    echo '<option selected value="'.$item.'">'.$item.' task</option>';
                                } else {
                                    echo '<option value="'.$item.'">'.$item.' task</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="preview-title-post">Chọn thời gian thu thập bài tự động</label>
                        <select id="post-schedule-time" name="post-schedule-time" class="form-control">
                            <?php
                            foreach($array_post_time as $time) {
                                if(get_option('post-schedule-time') && get_option('post-schedule-time') == $time*60) {
                                    echo '<option selected value="'.$time.'">'.$time.' phút</option>';
                                } else {
                                    echo '<option value="'.$time.'">'.$time.' phút</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="preview-title-post">Chọn số bài trong 1 lần thu thập</label>
                        <select id="post-schedule-count" name="post-schedule-count" class="form-control">
                            <?php
                            foreach($array_post_count as $item) {
                                if(get_option('post-schedule-count') && get_option('post-schedule-count') == $item) {
                                    echo '<option selected value="'.$item.'">'.$item.' post</option>';
                                } else {
                                    echo '<option value="'.$item.'">'.$item.' post</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>


                </div>

                <!-- /.panel-body -->
                <div class="panel-footer"> 
                    <button id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default">Lưu thay đôi</button>
                </div>
            </div>

        </div>
        </form>
    </div>
	<!-- /.col-lg-12 -->
    <?php
}