<?php
if (isset($_GET['task_id'])) {
$task_id = $_GET['task_id'];
$task_current = get_task_by_id($task_id);
$unjson = json_decode($task_current, true);
$unjson = $unjson[0];
}
$taxonomy = array();
$token_task_edit = isset($_GET['task_id']) ? $unjson['id'] : 0; 
?>
<div class="col-lg-11">
    <h1 class="page-header">Auto Crawler - Setting : Example Task - As a reference, you can quickly master use of this plugin</h1>
</div>
<!-- /.row -->
<div class="col-lg-11">
    <div class="panel panel-primary">
        <div data-target="#config-task" class="panel-heading " data-toggle="collapse" aria-expanded="true">
            Basic Setting
        </div>
        <!-- /.panel-heading -->
        <div id="config-task" class="panel-body collapse in" aria-expanded="true">
            <form class="form-horizontal">
                <div class="col-sm-12 form-group">
                    <label for="task-name" class="col-sm-2 control-label">Task Name:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="task-name" placeholder="Example Task - As a reference, you can quickly master use of this plugin" value="<?php echo $unjson['title']; ?>">
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="post-type" class="col-sm-2 control-label">Post Type:</label>
                    <div class="col-sm-2">
                        <select id="post-type-radio" class="form-control">
                            <?php
                            $args = array(
                                'public'   => true,
                                '_builtin' => true,
                            );

                            $output = 'names'; // names or objects, note names is the default
                            $operator = 'and'; // 'and' or 'or'

                            $post_types = get_post_types($args, $output, $operator);
                            foreach ($post_types as $post_type) { ?>
                                <option <?php if ($post_type == $unjson['post_type']) {
                                                echo 'selected';
                                            } ?> value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="posttype" class="col-sm-2 control-label">Taxonomy:</label>
                    <div class="col-sm-9">
                        <?php
                        $args = array(
                            'public'   => true,
                            '_builtin' => true,
                            'hierarchical' => true,

                        );
                        $output = 'names'; // or objects
                        $operator = 'and'; // 'and' or 'or'
                        $taxonomies = get_taxonomies($args, $output, $operator);
                        ?>
                        <ul id="taxonomy-list" class="list:<?php echo $taxonomy; ?> categorychecklist form-no-clear"><strong>Categories</strong>
                            <?php
                            $terms = get_terms(array(
                                'taxonomy' => 'category',
                                'hide_empty' => false,
                            ));
                            foreach ($terms as $term) :
                                ?>
                                <li id="<?php echo $taxonomy; ?>-1" class="popular-category">
                                    <label class="selectit">
                                        <input value="<?php echo $term->name; ?>" type="checkbox" <?php if (strpos($unjson['taxonomy'], $term->name) === false) { } else {
                                                                                                            echo 'checked';
                                                                                                        } ?> name="post_category[]" id="in-category-1"><?php echo $term->name; ?>
                                    </label>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="posttype" class="col-sm-2 control-label">Author:</label>
                    <div class="col-sm-2">
                        <select id="author" class="form-control">
                            <?php

                            $users = get_users($args);
                            foreach ($users as $user) { ?>
                                <option value="<?php echo $user->ID; ?>"><?php echo $user->user_login; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="updateinterval" class="col-sm-2 control-label">Update Interval:</label>
                    <div class="col-sm-1">
                        <input type="number" class="form-control" id="update_interval" name="update_interval" placeholder="Interval Minutes" value="<?php echo $unjson['update_interval']; ?>">
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="publisheinterval" class="col-sm-2 control-label">Published Date Interval:</label>
                    <div class="col-sm-1">
                        <input type="number" class="form-control" id="publish_interval" name="publish_interval" placeholder="Published Date Interval" value="<?php echo $unjson['publish_date']; ?>">
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="set_featured_image" class="col-sm-2 control-label">Set Image Future:</label>
                    <div class="col-sm-1">
                        <select id="set_featured_image" name="set_featured_image">
                            <option <?php if ($unjson['feature_image'] == 0) {
                                        echo 'selected';
                                    } ?> value="0">No</option>
                            <option <?php if ($unjson['feature_image'] == 1) {
                                        echo 'selected';
                                    } ?> value="1">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="posts_cheduled" class="col-sm-2 control-label">Post Scheduled:</label>
                    <div class="col-sm-1">
                        <select id="post_scheduled" name="post_scheduled" class="form-control">
                            <option <?php if ($unjson['post_scheduled'] == 0) {
                                        echo 'selected';
                                    } ?> value="0">No</option>
                            <option <?php if ($unjson['post_scheduled'] == 0) {
                                        echo 'selected';
                                    } ?> value="1">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="publish_status" class="col-sm-2 control-label">Publish Status:</label>
                    <div class="col-sm-1">
                        <select id="publish_status" name="publish_status" class="form-control">
                            <option <?php if ($unjson['publish_status'] == 'draft') {
                                        echo 'selected';
                                    } ?> value="draft">Draft</option>
                            <option <?php if ($unjson['publish_status'] == 'publish') {
                                        echo 'selected';
                                    } ?> value="publish">Published</option>

                            <option <?php if ($unjson['publish_status'] == 'pending') {
                                        echo 'selected';
                                    } ?> value="pending">Pending Review</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="data_spin_articles" class="col-sm-2 control-label">Data Spin Articles:</label>
                    <div class="col-sm-1">
                        <select id="data_spin_articles" name="data_spin_articles" class="form-control">
                            <option value="0" selected="true">None</option>
                            <?php

                            $datas =  get_list_data();
                            foreach ($datas as $data) { ?>
                                <option value="<?php echo $data->id; ?>"><?php echo $data->data_name; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.panel-body -->

    </div>
    <!-- /.panel -->
    <!-- /.panel Article Source Settings -->
    <div class="panel panel-primary">
        <div data-target="#article_source_settings" class="panel-heading" data-toggle="collapse">
            Article Source Settings
        </div>
        <!-- /.panel-heading -->
        <div id="article_source_settings" class="panel-body collapse">
            <div class="col-sm-12 form-group">
                <label for="urls" class="control-label">The URL of Article List:</label>
                <textarea name="urls" id="urls" rows="8" class="form-control"><?php echo $unjson['url_content']; ?></textarea>
            </div>
            <div class="col-sm-12 form-group">
                <label for="a_selector" class="col-sm-3 control-label">The Article URLs CSS Selector: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="a_selector" name="a_selector" placeholder="" value="<?php echo $unjson['url_selector']; ?>">
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="list_url_articles_test" class="col-sm-3 control-label">List URL Test: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="list_url_articles_test" name="list_url_articles_test" placeholder="">
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <button id="test_get_list_articles" type="button" class="btn btn-success">Test</button>
            </div>
            <div id="list-link-auto-post"></div>
        </div>
    </div>
    <!-- /.panel Article Source Settings -->
    <div class="panel panel-primary">
        <div data-target="#article_extraction_settings" class="panel-heading" data-toggle="collapse">
            Article Extraction Settings
        </div>
        <!-- /.panel-heading -->
        <div id="article_extraction_settings" class="panel-body collapse">

            <div class="col-sm-12 form-group">
                <label for="title_selector" class="col-sm-3 control-label">The Article Title CSS Selector: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="title_selector" name="title_selector" placeholder="" value="<?php echo $unjson['title_selector']; ?>">
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="content" class="col-sm-3 control-label">The Article Content CSS Selector: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="content_selector" name="content_selector" placeholder="" value="<?php echo $unjson['content_selector']; ?>">
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="image_selector" class="col-sm-3 control-label">Image Selector: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="image_selector" name="image_article" placeholder="" value="<?php echo $unjson['img_selector']; ?>">
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="urls" class="col-sm-3 control-label">Exclude HTML:</label>
                <div class="col-sm-5">
                    <textarea name="exclude_html" id="exclude_html" rows="8" class="form-control"><?php echo $unjson['exclude_html']; ?></textarea>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="url_articles_test" class="col-sm-3 control-label">URL Test: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="url_articles_test" name="url_articles_test" placeholder="">
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <button id="test_get_articles" type="button" class="btn btn-success">Test</button>
            </div>
            <div id="articles-auto-post">
                <span>Title : </span>
                <h2 class="title-post"></h2>
                <br>
                <span>Nội dung : </span>
                <br>
                <div class="content-post">
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-12 form-group">
        <input type="hidden" name="token_task_edit" id="token_task_edit" value="<?php echo $token_task_edit;?>">
        <button id="save-task" type="button" class="btn btn-success">Save Config Task</button>
    </div>
</div>
<!-- /.col-lg-12 -->