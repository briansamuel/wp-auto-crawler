<?php 


add_action( 'init', 'wptuts_register_activity_log_table', 1 );
add_action( 'switch_blog', 'wptuts_register_activity_log_table' );

function wptuts_register_activity_log_table() {
  	global $wpdb;
  	$wpdb->post_crawler = "{$wpdb->prefix}post_crawler";
  	$wpdb->task_crawler = "{$wpdb->prefix}task_crawler";
	$wpdb->spin_crawler = "{$wpdb->prefix}spin_crawler";
	$wpdb->logs_crawler = "{$wpdb->prefix}logs_crawler";
}

function wp_post_crawler_tables() {
  	global $wpdb;
  	global $charset_collate;
  	wptuts_register_activity_log_table ();

  	$sql_create_table = "CREATE TABLE IF NOT EXISTS {$wpdb->post_crawler} (
  		id int(20) unsigned NOT NULL auto_increment,
  		title text NOT NULL,
		url_post text NOT NULL,
		thumbnail_post text NOT NULL,
  		task_id int(20) unsigned NOT NULL,
		post_status varchar(20) NOT NULL default 'none',
  		post_update_at timestamp NOT NULL,
		post_date timestamp NOT NULL default CURRENT_TIMESTAMP,
  		PRIMARY KEY  (id)
  		) $charset_collate; ";
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql_create_table );

}

// Create table Task Crawler
function wp_task_crawler_tables() {
  	global $wpdb;
  	global $charset_collate;
  	wptuts_register_activity_log_table ();

  	$sql_create_table = "CREATE TABLE IF NOT EXISTS {$wpdb->task_crawler} (
  		id int(20) unsigned NOT NULL auto_increment,
  		title text NOT NULL,
      taxonomy text NOT NULL,
      post_type text NOT NULL,
  		url_content text NOT NULL,
  		url_selector text NOT NULL,
  		title_selector text NOT NULL,
  		content_selector text NOT NULL,
      img_selector text NOT NULL,
	  exclude_html text,
  		remove_link int(1) unsigned NOT NULL default 1,
  		feature_image int(1) unsigned NOT NULL default 1,
      update_interval int(2) unsigned NOT NULL default 60,
      publish_date int(2) unsigned NOT NULL default 60,
      post_scheduled int(1) unsigned NOT NULL default 0,
      publish_status text NOT NULL,
      data_spin int(5) unsigned NOT NULL default 0,
	  json_type int(1)  unsigned NOT NULL default 0,
	  json_content text,
	  task_update_at timestamp NOT NULL,
  		task_date timestamp NOT NULL default CURRENT_TIMESTAMP,
  		PRIMARY KEY  (id)
  		) $charset_collate; ";
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql_create_table );

}

// Create table Data Spin Crawler
function wp_dataspin_crawler_tables() {
  	global $wpdb;
  	global $charset_collate;
  	wptuts_register_activity_log_table ();

  	$sql_create_table = "CREATE TABLE IF NOT EXISTS {$wpdb->spin_crawler} (
  		id int(20) unsigned NOT NULL auto_increment,
  		data_name text NOT NULL,
  		data_content longtext NOT NULL,
  		data_date timestamp NOT NULL default CURRENT_TIMESTAMP,
  		PRIMARY KEY  (id)
  		) $charset_collate; ";
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql_create_table );

}

function wp_logs_crawler_tables() {
	global $wpdb;
	global $charset_collate;
	wptuts_register_activity_log_table ();

	$sql_create_table = "CREATE TABLE IF NOT EXISTS {$wpdb->logs_crawler} (
		id int(20) unsigned NOT NULL auto_increment,
		task_id int(20) unsigned NOT NULL,
		logs_content longtext NOT NULL,
		update_time timestamp,
		data_date timestamp NOT NULL default CURRENT_TIMESTAMP,
		PRIMARY KEY  (id)
		) $charset_collate; ";
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql_create_table );

}
?>