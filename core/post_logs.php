<?php 

// Thêm Auto link spin post
function wp_insert_autolinkpost($task_id,$title)
{
	global $wpdb;
	$t=time();
	$result = $wpdb->insert($wpdb->post_crawler, array(
		"task_id" => $task_id,
		"title" => $title,
		));

	return $result;
}
// END Thêm Auto link spin post
function get_post_logs($args = null)
{
	$offset = isset($args['offset']) ?  $args['offset'] : 0;
	$limit = isset($args['limit']) ?  $args['limit'] : 20;
	$post_status = isset($args['post_status']) ? $args['post_status'] : '';
	global $wpdb;
	$result = $wpdb->get_results( "SELECT * FROM {$wpdb->post_crawler} WHERE post_status LIKE '%{$post_status}%' LIMIT {$offset},{$limit}");
	return $result;
}

function count_all_post() {
	global $wpdb;
	$result = $wpdb->get_var("SELECT COUNT(*)  FROM {$wpdb->post_crawler}");
	return $result;
}
function get_post_log_by_id()
{
	# code...
}

function update_post_logs($args = null) {
	$id_post = isset($args['id']) ? $args['id'] : 0;
	
	global $wpdb;
  	$result = $wpdb->update($wpdb->post_crawler, $args,array( 'id' => $id_post ));
  	return $result;
}
function Delete_CrawlPost($id)
{
	global $wpdb;
	$result = $wpdb->delete( $wpdb->post_crawler, array( 'id' => $id ) );
	if($result)
	{
		return 'Success';
	}
	else{
		return 'Fail';
	}
}
?>