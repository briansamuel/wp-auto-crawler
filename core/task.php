<?php 
// Insert Task
function wp_insert_task($args = null)
{

  global $wpdb;
  $result = $wpdb->insert($wpdb->task_crawler, $args);
  if($result)
  {
    return $wpdb->insert_id;
  }
  else
  {
    return $result;
  }
  
}

// Update Task
function wp_update_task($id_task, $args = null)
{
  global $wpdb;
  $result = $wpdb->update($wpdb->task_crawler, $args ,array( 'id' => $id_task ));
  return $result;
  
}
// get Task by ID
function get_task_by_id($id) {

  global $wpdb;
  $result = $wpdb->get_results( "SELECT * FROM {$wpdb->task_crawler} WHERE id = {$id} LIMIT 0,1 ");
  return json_encode($result);
  
}
// End Task by ID
function get_task($args = null) {

  global $wpdb;
  $offset = isset($args['offset']) ?  $args['offset'] : 0;
	$limit = isset($args['limit']) ?  $args['limit'] : 20;
  $result = $wpdb->get_results( "SELECT * FROM {$wpdb->task_crawler} ORDER BY task_update_at ASC LIMIT {$offset},{$limit}");
  return $result;
  
}
// Xóa Task
function DeleteTaskPost($id)
{
  global $wpdb;
  $result = $wpdb->delete( $wpdb->task_crawler, array( 'id' => $id ) );
  if($result)
  {
    return 'Success';
  }
  else{
    return 'Fail';
  }
}
?>