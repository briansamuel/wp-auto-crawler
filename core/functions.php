<?php 

// Method check Exist Post Crawler
function checkExistCrawler($url) {
    global $wpdb;
    $row = $wpdb->get_results( "SELECT * FROM {$wpdb->post_crawler} WHERE url_post = '{$url}'");
    if(count($row) > 0)
        return true;
    else 
        return false;
}

function CrawlPost($task_id, $image, $url) {
    $task = get_task_by_id($task_id);
	$task = json_decode($task);
	$titselect = $task[0]->title_selector;
	$artselect = $task[0]->content_selector;
	$imgselect = $task[0]->img_selector;
	$hasFeature = $task[0]->feature_image;
	$taxonomy =  $task[0]->taxonomy;
    $data_spin =  $task[0]->data_spin;
    $remove_link =  $task[0]->remove_link;
    $exclude_html =  $task[0]->exclude_html;
	$taxonomy_args = explode(',', $taxonomy);
	$post_type =  $task[0]->post_type;
    $post_status =  $task[0]->publish_status;
    $data = ViewSource($url);
	$html = new simple_html_dom();
    $html->load($data);
    //$excude_array = array('.share', '.chat', '.author', '.fb-comments', '.my-2');
    $excude_array = explode("\n", $exclude_html);
    
    //$html->load($content);
    foreach($excude_array as $ex) {
        foreach($html->find($ex) as $item) {
            $item->outertext = '';
        }
    }
    
    $html->save();
    $parse = parse_url($url);
	$domain = $parse['host'];
	$titles = $html->find($titselect);
	$title = $titles[0]->innertext;
	$contents = $html->find($artselect);
	
    $content = $contents[0]->innertext;
    $content = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $content);
    $image = substr($image, 0, strpos($image, "&"));
    $images = getImages($content);
    $content = replaceImage($content, $images, $domain);
    if($remove_link == 1) {
        $content =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content);


    }
    $post_id = CreatePostWP($title,$image,$content,$taxonomy_args,$post_type,$post_status);
    if($post_id)
    {
        return $post_id;
    }
    else
    {
        return false;
    }
}
function crawlUrls($task_id) {
	$task = get_task_by_id($task_id);
	$task = json_decode($task);
	$task = $task[0];
    $urls = $task->url_content;
	$list_urls = explode("\n", $urls);
	$url = $list_urls[0];
	$parse = parse_url($url);
	$domain = $parse['host'];
	$json_type = $task->json_type;
	$json_content = $task->json_content;
	foreach ($list_urls as $url) {

		$data = ViewSource($url);
		$html = new simple_html_dom();
		if($json_type === '1') {
			$data = json_decode($data, true);
			if($json_content != '') {
				$data = $data[$json_content];
			}
			$html->load($data);
		} else {
			$html->load($data);
		}
	  	
		$img = '';
		$images = $html->find($task->img_selector);
		$links = $html->find($task->url_selector);
		//$titles = $html->find($task->title_selector);
		foreach($images as $key => $element) 
		{

				

				if($element->src)
				{
					//print_r($element->src);

					if (strpos($element->src, 'base64') !== false) {
						if (strpos($element->getAttribute('data-original'), $domain) !== false) {
							$img = $element->getAttribute('data-original');
						} else {
							$img = $domain.$element->getAttribute('data-original');
						}
					} else {
						
						if (strpos($element->src, $domain) !== false) {
							$img = $element->src;
						} else {
							$img = $domain.$element->src;
						}
					}
					//$title = trim($titles[$key]->plaintext);
					
					
					global $wpdb;
					if(!checkExistCrawler($links[$key]->href) && $img != '') {
						$result = $wpdb->insert($wpdb->post_crawler, array(
							"task_id" => $task_id,
							"title" => '',
							"url_post" => $links[$key]->href,
							"thumbnail_post" => $img,
						));
					}
					
					
				}
				
		}
	}
	
	//die();
}
function getImages($html) {
    $re = '/< *img[^>]*src *= *["\']?([^"\']*)/i';
    preg_match_all($re, $html, $matches);
    $src = array_pop($matches);

    return $src;
}
function replaceImage($content, $images, $domain) {
    foreach($images as $image) {
        if (strpos($image, $domain) !== false) {
            $img = $image;
        } else {
            $img = 'http://'.$domain.$image;
            $content = str_replace($image,$img,$content);
        }
        
    }
    return $content;
}
?>