<?php 

// Insert Data Spin
function wp_insert_dataspin($name,$content)
{
  global $wpdb;
  $t=time();
  $user_ID = get_current_user_id();

  $result = $wpdb->insert($wpdb->spin_crawler, array(
    "data_name" => $name,
    "data_content" => $content,
    'data_date' => $t
    ));
  if($result)
  {
    return $wpdb->insert_id;
  }
  else
  {
    return $result;
  }
  
}

function get_data_spin_by_user($user_id = 0)
{
	
	global $wpdb;
	$result = $wpdb->get_results( "SELECT * FROM {$wpdb->spin_crawler}");

	return $result;
}
// Lấy danh sách data SPIN theo User
// Lấy danh sách data SPIN theo User
function get_data_spin_by_id($id = 0)
{
	
	global $wpdb;
	if($id != 0)
	{
		$result = $wpdb->get_results( "SELECT * FROM {$wpdb->spin_crawler} WHERE id={$id} LIMIT 0,1");
	}
	else
	{
		$result = false;
	}

	return $result;
}

function get_list_data()
{
	
	global $wpdb;
	$result = $wpdb->get_results( "SELECT * FROM {$wpdb->spin_crawler}");
	return $result;
}

// Function update DataSpin
function update_data_spin_by_id($id,$name,$data)
{
	global $wpdb;
	$result = $wpdb->update($wpdb->spin_crawler, array(
		"data_name" => $name,
		"data_content" => $data,
		),
		array('id' => $id ));
	return $result;
}
// Function Delete DataSpin
function delete_data_spin_by_id($id)
{
	global $wpdb;
	$result = $wpdb->delete($wpdb->spin_crawler,
		array('id' => $id ));
	return $result;
}


?>