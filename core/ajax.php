<?php 

function ViewSource($url){
    $ch      = curl_init();
    $timeout = 300;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.69 Safari/537.36");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch,CURLOPT_TIMEOUT,300000);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	
    curl_close($ch);
    return $data;
}

add_action('wp_ajax_nopriv_ajax_insert_dataspin', 'set_ajax_insert_dataspin');
add_action('wp_ajax_ajax_insert_dataspin', 'set_ajax_insert_dataspin');
function set_ajax_insert_dataspin()
{
	
	$name = $_POST['name'];
	$content = $_POST['content'];
	$result = wp_insert_dataspin($name,$content);
	if($result)
	{
		
		echo 'Success';

	}
	else
	{
		echo 'Fail';
	}
	die();


}

/// AJAX Load dataspin
add_action('wp_ajax_nopriv_ajax_spin_load', 'set_ajax_spin_load');
add_action('wp_ajax_ajax_spin_load', 'set_ajax_spin_load');
function set_ajax_spin_load()
{
	$id = $_POST['id'];
	$result = get_data_spin_by_id($id);
	$data = $result[0]->data_content;
	$name = $result[0]->data_name;
	$args  = array($name,$data);
	echo json_encode($args);
	die();


}

/// AJAX Update dataspin
add_action('wp_ajax_nopriv_ajax_spin_update', 'set_ajax_spin_update');
add_action('wp_ajax_ajax_spin_update', 'set_ajax_spin_update');
function set_ajax_spin_update()
{
	$id = $_POST['id'];
	$name = $_POST['name'];
	$data = $_POST['data'];
	$result = update_data_spin_by_id($id,$name,$data);
	if($result)
	{
		echo 'Đã cập nhật Data Spin';
	}
	else
	{
		echo 'Lỗi cập nhật';
	}
	die();


}
/// AJAX Delete dataspin
add_action('wp_ajax_nopriv_ajax_spin_delete', 'set_ajax_spin_delete');
add_action('wp_ajax_ajax_spin_delete', 'set_ajax_spin_delete');
function set_ajax_spin_delete()
{
	$id = $_POST['id'];
	$result = delete_data_spin_by_id($id);
	if($result)
	{
		echo 'Đã Xóa Data Spin';
	}
	else
	{
		echo 'Lỗi Xóa Data Spin';
	}
	die();


}
/// AJAX List Url from Task
add_action('wp_ajax_nopriv_ajax_get_url', 'set_ajax_get_url');
add_action('wp_ajax_ajax_get_url', 'set_ajax_get_url');
function set_ajax_get_url()
{
	$url = $_POST['url'];
	$linkselect = $_POST['linkcss'];
	$data = ViewSource($url);
  	$html = new simple_html_dom();
  //$html = file_get_html($url);
  	$html->load($data);
  	$result = $html->find($linkselect);
	$array_link = array();
	$parse = parse_url($url);
	$domain = 'http://'.$parse['host'].'/';
	foreach($result as $element) 
	{
			//echo $element->href . '<br>';

			if(strpos($element->href, $parse['host']) !== false)
			{
				$domainfix = $element->href;
			}
			else
			{
				$domainfix = $domain.$element->href;
			}
	        array_push($array_link,$domainfix);
	}
	$links = array_unique($array_link);
	echo json_encode($links);
	die();
}

/// AJAX get Articles
add_action('wp_ajax_nopriv_ajax_get_post', 'set_ajax_get_post');
add_action('wp_ajax_ajax_get_post', 'set_ajax_get_post');
function set_ajax_get_post()
{
	$url = $_POST['url'];
	$titselect = $_POST['titcss'];
	$artselect = $_POST['artcss'];
	
	//$article_selector_arr = explode(',',$artselect);
	$exclude_html = $_POST['exclude_html'];
	$excude_array = explode("\n",$exclude_html);
	$data = ViewSource($url);
  	$html = new simple_html_dom();
  	$html->load($data);
	$parse = parse_url($url);
	$domain = 'http://'.$parse['host'].'/';
	
	$content_post = '';
	$title_post = '';
	$titles = $html->find($titselect);
	$title = $titles[0];
	$title = strip_tags($title);
	foreach($excude_array as $ex) {
        foreach($html->find($ex) as $item) {
            $item->outertext = '';
        }
    }
	$contents = $html->find(trim($artselect));
	$content = $contents[0];
	$content =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content);
	
	echo $content.'|-|-|'.$title;
	die();
}

//
// Ajax Add Task
add_action('wp_ajax_nopriv_ajax_insert_task', 'set_ajax_insert_task');
add_action('wp_ajax_ajax_insert_task', 'set_ajax_insert_task');

function set_ajax_insert_task() {
	global $wpdb;
	$title = $_POST['title'];
	$taxonomy = $_POST['taxonomy'];
	$post_type = $_POST['post_type'];
	$author = $_POST['author'];
	$publish_status = $_POST['publish_status'];
	$data_spin = $_POST['data_spin_articles'];
	$url_content = $_POST['list_url'];
	$url_selector = $_POST['url_selector'];
	$title_selector = $_POST['title_selector'];
	$content_selector = $_POST['content_selector'];
	$img_selector = $_POST['img_selector'];
	$taxonomy = implode(',', $taxonomy);
	$exclude_html = $_POST['exclude_html'];
	$json_type = $_POST['json_type'];
	$json_content = $_POST['json_content'];
	$token_task_edit = $_POST['token_task_edit'];
	$args = array(
		"title" => $title,
		"taxonomy" => $taxonomy, // Trường này chưa có
		"post_type" => $post_type,
		"url_content" => $url_content,
		"url_selector" => $url_selector,
		"title_selector" => $title_selector,
		"content_selector" => $content_selector,
		"img_selector" => $img_selector,
		"remove_link" => 1,
		"feature_image" => 1,
		"update_interval" => 60,
		"publish_date" => 60,
		"post_scheduled" => 1,
		"publish_status" => $publish_status,
		"data_spin" => $data_spin,
		"exclude_html" => $exclude_html,
		"json_type" => $json_type,
		"json_content" => $json_content,
		"task_update_at" => date('Y-m-d h:i:s', time()),
	);
	if($token_task_edit != 0)
	{
		$result = wp_update_task($token_task_edit, $args);
	}
	else
	{
		
		$result = wp_insert_task($args);
	}
	if($result)
	{
		if($token_task_edit != 0)
		{
			echo 'Cập nhật Task thành công';
		}
		else
		{
			echo 'Thêm Task mới thành công';
		}
		

	}
	else
	{
		echo $wpdb->last_error;
	}
	die();
}
// End Ajax Add Task
// Ajax Get Task
add_action('wp_ajax_nopriv_ajax_get_task', 'set_ajax_get_task');
add_action('wp_ajax_ajax_get_task', 'set_ajax_get_task');

function set_ajax_get_task() {

	global $wpdb;
	$result = $wpdb->get_results( "SELECT id,title FROM {$wpdb->task_crawler} ");
	return json_encode($result);
	
}
// END AJAX GET TASK
// AJAX Get List Post From Task 
add_action('wp_ajax_nopriv_ajax_get_list_post_by_task', 'get_list_post_by_task');
add_action('wp_ajax_ajax_get_list_post_by_task', 'get_list_post_by_task');
function get_list_post_by_task()
{
	$id = $_POST['id'];
	$task = get_task_by_id($id);
	$task = json_decode($task);
	$urls = $task[0]->url_content;
	$list_urls = explode("\n", $urls);
	$url = $list_urls[0];
	$parse = parse_url($url);
	$domain = $parse['host'];
	$json_type = $task[0]->json_type;
	$json_content = $task[0]->json_content;
	$post_url_arr = array();
	foreach ($list_urls as $url) {
		$data = ViewSource($url);
		$html = new simple_html_dom();
		if($json_type === '1') {
			$data = json_decode($data, true);
			if($json_content != '') {
				$data = $data[$json_content];
			}
			$html->load($data);
		} else {
			$html->load($data);
		}

		$img = '';
		$images = $html->find($task[0]->img_selector);
		$links = $html->find($task[0]->url_selector);
		
		foreach($images as $key => $element) 
		{

				

				if($element->src)
				{
					//print_r($element->src);
					
					if (strpos($element->src, $domain) !== false) {
						$img = $element->src;
					} else {
						$img = $domain.$element->src;
					}
					$string_code = $links[$key]->href.'|-|'.$img;
					array_push($post_url_arr, $string_code);
				}
				
		}
	}

	echo json_encode($post_url_arr);
	die();
	
}
// Ajax Crawl Post
add_action('wp_ajax_nopriv_ajax_crawl_post', 'set_ajax_crawl_post');
add_action('wp_ajax_ajax_crawl_post', 'set_ajax_crawl_post');
function set_ajax_crawl_post() {
	$id = $_POST['id'];
	$string_code = $_POST['url'];
	$arr_code = explode('|-|',$string_code);
	$link = isset($arr_code[1]) ? $arr_code[0] : '';
	$image = isset($arr_code[2]) ? $arr_code[1] : '';
	global $wpdb;
	if(!checkExistCrawler($link) && $image != '') {
		$result = $wpdb->insert($wpdb->post_crawler, array(
			"task_id" => $id,
			"title" => '',
			"url_post" => $link,
			"thumbnail_post" => $image,
		));
		if($result) {
			echo '<p class="text-success font-weight-bold"><label>'.$link. ' === Thu thập URL thành công === </label></p>';
		} else {
			echo '<p class="text-danger font-weight-bold"><label>'.$link. ' === Thu thập URL thất bại === </label></p>';
		}
	} else {
		echo '<p class="text-warning font-weight-bold"><label>'.$link.' === Url này đã tồn tại === </label></p> ';
	}
	
	die();
}
// END Ajax Crawl Post
// Ajax Auto Create Post
add_action('wp_ajax_nopriv_ajax_auto_create_post', 'set_ajax_auto_create_post');
add_action('wp_ajax_ajax_auto_create_post', 'set_ajax_auto_create_post');

function set_ajax_auto_create_post() {

	$id = $_POST['id'];
	$task = get_task_by_id($id);
	$task = json_decode($task);
	$urls = $task[0]->url_content;
	$list_urls = explode("\n", $urls);
	$url = $list_urls[0];
	$parse = parse_url($url);
	$domain = $parse['host'];
	$json_type = $task[0]->json_type;
	$json_content = $task[0]->json_content;
	foreach ($list_urls as $url) {

		$data = ViewSource($url);
		$html = new simple_html_dom();
		if($json_type === '1') {
			$data = json_decode($data, true);
			if($json_content != '') {
				$data = $data[$json_content];
			}
			$html->load($data);
		} else {
			$html->load($data);
		}
	  	
		$img = '';
		$images = $html->find($task[0]->img_selector);
		$links = $html->find($task[0]->url_selector);
		$titles = $html->find($task[0]->title_selector);
		foreach($images as $key => $element) 
		{

				

				if($element->src)
				{
					//print_r($element->src);
					$title = trim($titles[$key]->plaintext);
					if (strpos($element->src, $domain) !== false) {
						$img = $element->src;
					} else {
						$img = $domain.$element->src;
					}
					
					global $wpdb;
					if(!checkExistCrawler($links[$key]->href,$title) && $img != '' && $title != '') {
						$result = $wpdb->insert($wpdb->post_crawler, array(
							"task_id" => $id,
							"title" => $title,
							"url_post" => $links[$key]->href,
							"thumbnail_post" => $img,
						));
					}
					
					
				}
				
		}
	}
	
	
	die();
	
}

// End Ajax Auto Create Post
// Ajax Auto Create Post WP
add_action('wp_ajax_nopriv_auto_create_post_wp', 'set_ajax_auto_create_post_wp');
add_action('wp_ajax_ajax_auto_create_post_wp', 'set_ajax_auto_create_post_wp');

function set_ajax_auto_create_post_wp() {

	$id = $_POST['id'];
	$arr_url = explode('|',$_POST['url']);
	$task = get_task_by_id($id);
	$task = json_decode($task);
	$titselect = $task[0]->title_selector;
	$artselect = $task[0]->content_selector;
	$imgselect = $task[0]->img_selector;
	$hasFeature = $task[0]->feature_image;
	$taxonomy =  $task[0]->taxonomy;
	$data_spin =  $task[0]->data_spin;
	$taxonomy_args = explode(',', $taxonomy);
	$post_type =  $task[0]->post_type;
	$post_status =  $task[0]->publish_status;
	$url = $arr_url[0];
	$data = ViewSource($url);
	$html = new simple_html_dom();
	$html->load($data);
	
	$titles = $html->find($titselect);
	
	$parse = parse_url($url);
	$domain = 'http://'.$parse['host'].'/';
	$contents = $html->find($artselect);
	$content_text = '';
	$title_text = '';

	foreach($contents as $item) {

		
		$content = $item->innertext;	
	}

	foreach($titles as $title) {		
		$title_text = $title->plaintext;	
	}
	
	$img = $arr_url[1];
	$text =  html_entity_decode($title_text);
// Insert the post into the database

	global $wpdb;
	$row = $wpdb->get_results( "SELECT * FROM {$wpdb->post_crawler} WHERE title = '{$text}' ");
	if(count($row) > 0)
	{
		echo 'Bài viết này đã có trong Database';
	}
	else
	{

		
		if($data_spin != 0)
		{
				$article = RewriteArticles($data_spin,$text,$content);
		}
		else
		{
				$article = false;
		}
		if($article != false)
		{
				
				$title = SpinArticle($article[0]);
				$content = SpinArticle($article[1]);
				$content = strip_tags($content,'<br><p><img><div>');
				$result2 = CreatePostWP($title,$img,$content,$taxonomy_args,$post_type,$post_status);
				if($result2)
				{
					echo 'Lấy tin thành công có spin - '.$title;
					$result = wp_insert_autolinkpost($id,$text);
				}
				else
				{
					echo 'Lấy tin thất bại';
				}
		}
		else
		{
				//$content = strip_tags($content,'<ul><li><a><br><p><img><div><span>');
			    $content = preg_replace('#<div class="nhom-thongtin">(.*?)</div>#', '', $content);
				$result2 = CreatePostWP($text,$img,$content,$taxonomy_args,$post_type,$post_status);
				if($result2)
				{
					echo 'Lấy tin thành công - '.$text;
					wp_insert_autolinkpost($id,$text);
				}
				else
				{
					echo $wpdb->last_error;
				}
		}
		

	}
	die();
	
}


// Ajax Databtable Posts 
add_action('wp_ajax_nopriv_datatable_post', 'datatables_server_side_callback');
add_action('wp_ajax_datatable_post', 'datatables_server_side_callback');

function datatables_server_side_callback() {
	$data = array();
	$offset = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
	$litmit = isset($_REQUEST['length']) ? $_REQUEST['length'] : 20;
	$args = array(
		'offset' => $offset,
		'limit' => $litmit,
		
	);
	$posts = get_post_logs($args);
	$data['recordsTotal'] = count_all_post();
	$data['recordsFiltered'] = count_all_post();
	$data['data'] = $posts;
	echo json_encode($data);
	die();
}
?>