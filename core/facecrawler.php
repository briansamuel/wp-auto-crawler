<?php


function wac_settings_page_admin_menu()
{
	add_menu_page('Wp Auto Crawler', 'Wp Auto Crawler', 'manage_options', 'wp-autocrawler', 'list_task_options', 'dashicons-sos', 1);
	add_submenu_page('wp-autocrawler', 'Tắt cả chiến dịch', 'Tắt cả chiến dịch', 'manage_options', 'wp-autocrawler');

	add_submenu_page('wp-autocrawler', 'Thêm mới', 'Thêm mới', 'manage_options', 'wac-setting-page', 'wac_settings_page');
	add_submenu_page('wp-autocrawler', 'Quản lý', 'Quản lý', 'manage_options', 'wac-logs-page', 'list_logs_page');
	add_submenu_page('wp-autocrawler', 'Data Spin', 'Data Spin', 'manage_options', 'data-spin-page', 'data_spin_options');
	add_submenu_page('wp-autocrawler', 'Cài đặt', 'Cài đặt', 'manage_options', 'wp_crawler_setting_page', 'wp_crawler_setting_page');
}

function wac_settings_page()
{
	if (isset($_GET['task_id'])) {
		$task_id = $_GET['task_id'];
		$task_current = get_task_by_id($task_id);
		$unjson = json_decode($task_current, true);
		$unjson = $unjson[0];
		if(isset($_GET['action']) && $_GET['action'] == 'clone') {
			$unjson['id'] = 0;
		}
	}
	$taxonomy = array();
	?>
	<div class="col-lg-11">
		<h2 class="page-header">Auto Crawler - Setting : Example Task - As a reference, you can quickly master use of this plugin</h2>
	</div>
	<!-- /.row -->
	<form class="form-horizontal">
		<div class="col-lg-11">
			<div class="panel panel-primary">
				<div data-target="#config-task" class="panel-heading " data-toggle="collapse" aria-expanded="true">
					Basic Setting
				</div>
				<!-- /.panel-heading -->
				<div id="config-task" class="panel-body collapse in" aria-expanded="true">

					<div class="col-sm-12 form-group">
						<label for="task-name" class="col-sm-2 control-label">Task Name:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="task-name" required placeholder="Example Task - As a reference, you can quickly master use of this plugin" value="<?php echo $unjson['title']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="post-type" class="col-sm-2 control-label">Post Type:</label>
						<div class="col-sm-2">
							<select id="post-type-radio" class="form-control" required>
								<?php
									$args = array(
										'public'   => true,
										'_builtin' => true,
									);

									$output = 'names'; // names or objects, note names is the default
									$operator = 'and'; // 'and' or 'or'

									$post_types = get_post_types($args, $output, $operator);
									foreach ($post_types as $post_type) { ?>
									<option <?php if ($post_type == $unjson['post_type']) {
														echo 'selected';
													} ?> value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
								<?php
									}
									?>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="posttype" class="col-sm-2 control-label" required>Taxonomy:</label>
						<div class="col-sm-9">
							<?php
								$args = array(
									'public'   => true,
									'_builtin' => true,
									'hierarchical' => true,

								);
								$output = 'names'; // or objects
								$operator = 'and'; // 'and' or 'or'
								$taxonomies = get_taxonomies($args, $output, $operator);
								?>
							<ul id="taxonomy-list" class="list:<?php echo $taxonomy; ?> categorychecklist form-no-clear"><strong>Categories</strong>
								<?php
									$terms = get_terms(array(
										'taxonomy' => 'category',
										'hide_empty' => false,
									));
									foreach ($terms as $term) :
										?>
									<li id="<?php echo $taxonomy; ?>-1" class="popular-category">
										<label class="selectit">
											<input value="<?php echo $term->name; ?>" type="checkbox" <?php if (strpos($unjson['taxonomy'], $term->name) === false) { } else {
																													echo 'checked';
																												} ?> name="post_category[]" id="in-category-1"><?php echo $term->name; ?>
										</label>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="posttype" class="col-sm-2 control-label">Author:</label>
						<div class="col-sm-2">
							<select id="author" class="form-control">
								<?php

									$users = get_users($args);
									foreach ($users as $user) { ?>
									<option value="<?php echo $user->ID; ?>"><?php echo $user->user_login; ?></option>
								<?php
									}
									?>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="updateinterval" class="col-sm-2 control-label">Update Interval:</label>
						<div class="col-sm-1">
							<input type="number" class="form-control" id="update_interval" name="update_interval" placeholder="Interval Minutes" value="<?php echo $unjson['update_interval']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="publisheinterval" class="col-sm-2 control-label">Published Date Interval:</label>
						<div class="col-sm-1">
							<input type="number" class="form-control" id="publish_interval" name="publish_interval" placeholder="Published Date Interval" value="<?php echo $unjson['publish_date']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="set_featured_image" class="col-sm-2 control-label">Set Image Future:</label>
						<div class="col-sm-1">
							<select id="set_featured_image" name="set_featured_image">
								<option <?php if ($unjson['feature_image'] == 0) {
												echo 'selected';
											} ?> value="0">No</option>
								<option <?php if ($unjson['feature_image'] == 1) {
												echo 'selected';
											} ?> value="1">Yes</option>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="posts_cheduled" class="col-sm-2 control-label">Post Scheduled:</label>
						<div class="col-sm-1">
							<select id="post_scheduled" name="post_scheduled" class="form-control">
								<option <?php if ($unjson['post_scheduled'] == 0) {
												echo 'selected';
											} ?> value="0">No</option>
								<option <?php if ($unjson['post_scheduled'] == 1) {
												echo 'selected';
											} ?> value="1">Yes</option>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="publish_status" class="col-sm-2 control-label">Publish Status:</label>
						<div class="col-sm-1">
							<select id="publish_status" name="publish_status" class="form-control">
								<option <?php if ($unjson['publish_status'] == 'draft') {
												echo 'selected';
											} ?> value="draft">Draft</option>
								<option <?php if ($unjson['publish_status'] == 'publish') {
												echo 'selected';
											} ?> value="publish">Published</option>

								<option <?php if ($unjson['publish_status'] == 'pending') {
												echo 'selected';
											} ?> value="pending">Pending Review</option>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="data_spin_articles" class="col-sm-2 control-label">Data Spin Articles:</label>
						<div class="col-sm-1">
							<select id="data_spin_articles" name="data_spin_articles" class="form-control">
								<option value="0" selected="true">None</option>
								<?php

									$datas =  get_list_data();
									foreach ($datas as $data) { ?>
									<option value="<?php echo $data->id; ?>"><?php echo $data->data_name; ?></option>
								<?php
									}
									?>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="posts_cheduled" class="col-sm-2 control-label">JSON Type:</label>
						<div class="col-sm-1">
							<select id="json_type" name="json_type" class="form-control">
								<option <?php if ($unjson['json_type'] == 0) {
												echo 'selected';
											} ?> value="0">No</option>
								<option <?php if ($unjson['json_type'] == 1) {
												echo 'selected';
											} ?> value="1">Yes</option>
							</select>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="publisheinterval" class="col-sm-2 control-label">JSON Content:</label>
						<div class="col-sm-1">
							<input type="text" class="form-control" id="json_content" name="json_content" placeholder="Published Date Interval" value="<?php echo $unjson['json_content']; ?>">
						</div>
					</div>

				</div>
				<!-- /.panel-body -->

			</div>
			<!-- /.panel -->
			<!-- /.panel Article Source Settings -->
			<div class="panel panel-primary">
				<div data-target="#article_source_settings" class="panel-heading" data-toggle="collapse">
					Article Source Settings
				</div>
				<!-- /.panel-heading -->
				<div id="article_source_settings" class="panel-body collapse">
					<div class="col-sm-12 form-group">
						<label for="urls" class="control-label">The URL of Article List:</label>
						<textarea name="urls" id="urls" rows="8" class="form-control" required><?php echo $unjson['url_content']; ?></textarea>
					</div>
					<div class="col-sm-12 form-group">
						<label for="a_selector" class="col-sm-3 control-label">The Article URLs CSS Selector: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="a_selector" name="a_selector" required placeholder="" value="<?php echo $unjson['url_selector']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="list_url_articles_test" class="col-sm-3 control-label">List URL Test: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="list_url_articles_test" name="list_url_articles_test" placeholder="">
						</div>
					</div>

					<div class="col-sm-12 form-group">
						<button id="test_get_list_articles" type="button" class="btn btn-success">Test</button>
					</div>
					<div id="list-link-auto-post"></div>
				</div>
			</div>
			<!-- /.panel Article Source Settings -->
			<div class="panel panel-primary">
				<div data-target="#article_extraction_settings" class="panel-heading" data-toggle="collapse">
					Article Extraction Settings
				</div>
				<!-- /.panel-heading -->
				<div id="article_extraction_settings" class="panel-body collapse">

					<div class="col-sm-12 form-group">
						<label for="title_selector" class="col-sm-3 control-label">The Article Title CSS Selector: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="title_selector" name="title_selector" required placeholder="" value="<?php echo $unjson['title_selector']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="content" class="col-sm-3 control-label">The Article Content CSS Selector: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="content_selector" name="content_selector" required placeholder="" value="<?php echo $unjson['content_selector']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="image_selector" class="col-sm-3 control-label">Image Selector: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="image_selector" name="image_article" required placeholder="" value="<?php echo $unjson['img_selector']; ?>">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="urls" class="col-sm-3 control-label">Exclude HTML:</label>
						<div class="col-sm-5">
							<textarea name="exclude_html" id="exclude_html" required rows="8" class="form-control"><?php echo $unjson['exclude_html']; ?></textarea>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label for="url_articles_test" class="col-sm-3 control-label">URL Test: </label>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="url_articles_test" name="url_articles_test" placeholder="">
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<button id="test_get_articles" type="button" class="btn btn-success">Test</button>
					</div>
					<div id="articles-auto-post">
						<span>Title : </span>
						<h2 class="title-post"></h2>
						<br>
						<span>Nội dung : </span>
						<br>
						<div class="content-post">
						</div>
					</div>
				</div>

			</div>
			<div class="col-sm-12 form-group">
				<input type="hidden" name="token_task_edit" id="token_task_edit" value="<?php if (isset($_GET['task_id'])) {
																								echo $unjson['id'];
																							} else {
																								echo 0;
																							} ?>">
				<button id="save-task" type="button" class="btn btn-success">Save Config Task</button>
			</div>
		</div>
	</form>
	<!-- /.col-lg-12 -->
<?php
}

add_action('admin_menu', 'wac_settings_page_admin_menu');
// Edit Task Manager 


// Add Manager Spin Data
function data_spin_page_admin_menu()
{ }

function data_spin_options()
{
	?>
	<div class="col-lg-11">
		<h1 class="page-header">Manager Spin Data</h1>
	</div>
	<!-- /.row -->
	<div class="col-lg-11">
		<div class="panel panel-default">
			<div class="panel-heading">
				Spin Data Setting
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">

				<div class="form-group">
					<label for="preview-title-post">Chọn loại data spin (Bỏ qua bước này nếu bạn muốn thêm mới)</label>
					<select id="data-spin-select-mng" class="form-control">
						<option value="0">Thêm Mới</option>
						<?php
							$listdata = get_data_spin_by_user();
							foreach ($listdata as $data) {
								echo '<option value="' . $data->id . '">' . $data->data_name . '</option>';
							}
							?>
					</select>
				</div>
				<div class="form-group">
					<label for="preview-title-post">Tên Data Spin</label>
					<input type="text" class="form-control" id="title-data" placeholder="Tên Data Spin">
				</div>
				<div class="form-group">
					<label for="preview-post">Từ khóa spin (* Nhập theo cấu trúc mẫu dưới đây)</label>
					<textarea name="keyword-data" id="keyword-data" class="form-control" rows="15" placeholder="{keyword 1|keyword 2|keyword 3}"></textarea>

				</div>


			</div>

			<!-- /.panel-body -->
			<div class="panel-footer">
				<button id="add-data-spin" type="button" class="btn btn-default">Thêm</button>
				<button id="update-data-spin" type="button" class="btn btn-default" disabled="true">Cập nhật</button>
				<button id="delete-data-spin" type="button" class="btn btn-default" disabled="true">Xóa</button>
			</div>
		</div>

	</div>
	<!-- /.col-lg-12 -->
<?php
}

add_action('admin_menu', 'data_spin_page_admin_menu');

// List Task


function list_task_options()
{
	$alert = '';
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if (isset($_POST['deleteTask'])) {
			$list_crawl_select = $_POST['check-post'];
			//var_dump($list_task_select);
			$alert = 'Start';
			foreach ($list_crawl_select as $crawl) {
				//var_dump($task);
				$result = DeleteTaskPost($crawl);
				if ($result == 'Fail') {
					$alert .= 'Bài viết ID ' . $crawl . ' không xóa được <br>';
				}
			}
		}
	}
	?>
	<div class="row">
		<div class="col-lg-11">
			<h1 class="page-header">Quản lý task lấy tin</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-11">
			<?php if ($alert == 'Start') { ?>
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Đã Xóa Task thành công</strong>
				</div>
			<?php } else if ($alert != false) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong><?php echo $alert; ?></strong>
				</div>
			<?php } ?>
			<div class="panel panel-success">
				<div class="panel-heading">
					Danh sách các task
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<form action="" method="POST">
						<table id="table-task" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>

									<th>ID</th>
									<th>Task Name</th>
									<th>Update Now</th>
									<th>
										<input type="checkbox" name="check-all" id="check-all">
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$tasks = json_decode(set_ajax_get_task());
									//var_dump($tasks);
									foreach ($tasks as $task) :
										?>
									<tr role="row" class="odd">
										<td class="id-task">
											<?php echo $task->id; ?>
										</td>
										<td>
											<?php echo $task->title; ?>
										</td>
										<td>
											<button type="button" data-task="<?php echo $task->id; ?>" class="btn btn-success start-task">Thu thập thủ công</button>
											<a type="button" href="admin.php?page=wac-setting-page&task_id=<?php echo $task->id; ?>&action=clone" class="btn btn-success" style="margin-right: 4px">Sao chép Task</button>
											<a type="button" href="admin.php?page=wac-setting-page&task_id=<?php echo $task->id; ?>" class="btn btn-success">Sửa Task</button>
										</td>
										<td>
											<input type="checkbox" name="check-post[]" value="<?php echo $task->id; ?>" id="post[<?php echo $task->id; ?>]">
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="form-group">
							<div class="col-sm-2 col-sm-offset-10">
								<button id="deleteTask" name="deleteTask" type="submit" class="btn btn-primary">Xóa Task</button>
							</div>
						</div>
					</form>

				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>

	</div>
	<!-- /.col-lg-12 -->
	<div class="col-lg-11">
		<div class="panel panel-success">
			<div class="panel-heading">
				Danh sách link get được
			</div>

			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="progress">
					<div id="progress-crawler" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<br>
				<div class="list-post-auto">

				</div>
			</div>

		</div>
		<!-- /.panel -->

	</div>
	<!-- /.col-lg-12 -->
<?php
}



function list_logs_page()
{
	$alert = '';
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if (isset($_POST['deleteLogs'])) {
			$list_crawl_select = $_POST['check-post'];
			//var_dump($list_task_select);
			$alert = 'Start';
			foreach ($list_crawl_select as $crawl) {
				//var_dump($task);
				$result = Delete_CrawlPost($crawl);
				if ($result == 'Fail') {
					$alert .= 'Bài viết ID ' . $crawl . ' không xóa được <br>';
				}
			}
		}
	}
	?>
	<div class="row">
		<div class="col-lg-11">
			<h1 class="page-header">Quản lý bài viết thu thập</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-11">
			<?php if ($alert == 'Start') { ?>
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Đã Xóa log thành công</strong>
				</div>
			<?php } else if ($alert != false) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong><?php echo $alert; ?></strong>
				</div>
			<?php } ?>
			<div class="panel panel-success">
				<div class="panel-heading">
					Danh sách bài viết
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<form action="" method="POST">
						<table id="table-posts" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>

									<th>
										<input type="checkbox" name="check-all" id="check-all">
									</th>
									<th>
										ID
									</th>
									<th>Post Title</th>
									<th>URL</th>
									<th>Post Status</th>
									<th>Post Date</th>

								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
						<div class="form-group">
							<div class="col-sm-2 col-sm-offset-10">
								<button id="deleteLogs" name="deleteLogs" type="submit" class="btn btn-primary">Xóa bài viết</button>
							</div>
						</div>
					</form>

				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>

	</div>
	<!-- /.col-lg-12 -->
	<div class="col-lg-11">
		<div class="panel panel-success">
			<div class="panel-heading">
				Danh sách link get được
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body list-post-auto">
			</div>

		</div>
		<!-- /.panel -->

	</div>
	<!-- /.col-lg-12 -->
<?php
}

?>