<?php 


date_default_timezone_set('Asia/Ho_Chi_Minh');

// Custom 5 Minutes Request
add_filter( 'cron_schedules', 'cron_add_weekly' );
 
function cron_add_weekly( $schedules ) {
   // Adds once weekly to the existing schedules.
   $post_time = get_option('post-schedule-time') ? get_option('post-schedule-time') : 60;
   $task_time = get_option('task-schedule-time') ? get_option('task-schedule-time') : 300;
   $schedules['every_one_minutes'] = array(
       'interval' => $post_time,
       'display' => __( 'Every One Minutes' )
   );
   $schedules['every_five_minutes'] = array(
		'interval' => $task_time,
		'display' => __( 'Every Five Minutes' )
	);
   return $schedules;
}


if ( !wp_next_scheduled( 'wp_crawler_event' ) ) {
	wp_schedule_event(time(), 'every_one_minutes', 'wp_crawler_event');
}

if ( !wp_next_scheduled( 'crawl_list_post_event' ) ) {
	wp_schedule_event(time(), 'every_five_minutes', 'crawl_list_post_event');
}
 
add_action('wp_crawler_event', 'do_this_wp_crawler_event');
function do_this_wp_crawler_event() {
	$limit = get_option('post-schedule-count') ? get_option('post-schedule-count') : 1;
	$args = array(
		'offset' => 0,
		'limit' => $limit,
		'post_status' => 'none',
	);
	$posts = get_post_logs($args);
	if(count($posts) > 0) {
		$post = $posts[0];
		$result = CrawlPost($post->task_id, $post->thumbnail_post, $post->url_post);
		if($result) {
			$args = array(
				'id' => $post->id,
				'post_status' => 'crawled',
				'post_update_at' =>  date('Y-m-d H:i:s', time()),
			);
			$update_res = update_post_logs($args);
			return $update_res;
		}
		
	}
	return false;

}

add_action('crawl_list_post_event', 'do_crawl_list_post');
function do_crawl_list_post() {

	$limit = get_option('post-schedule-count') ? get_option('post-schedule-count') : 1;
	$args = array(
		'offset' => 0,
		'limit' => $limit,
		
	);
	$tasks = get_task($args);
	
	if(count($tasks) > 0) {
		foreach($tasks as $task) {
			$result = crawlUrls($task->id);
			$args = array('task_update_at' =>  date('Y-m-d H:i:s', time()));
			wp_update_task($task->id, $args);
		}
	}
}
?>