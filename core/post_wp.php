<?php 
date_default_timezone_set('Asia/Ho_Chi_Minh');
// END String to Spin Normal
function StringtoSpinNormal($array_spin,$content,$stringline)
{

	
	foreach ($array_spin as $key) {
		$string = implode('|', $array_spin);
		if (strpos($content, $key) !== false) {
			
			$content = str_replace(' '.$key.' ',' '.$stringline.' ',$content);

			
		}
	}
	return $content;
}
// END String to Spin Normal
function SpinArticle($string)
{
	return Spinner::nested($string, false);
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    $image_exist = wp_get_image_editor($filename);
	if (is_wp_error($image_exist)){
	    if(wp_mkdir_p($upload_dir['path']))     
     	  $file = $upload_dir['path'] . '/' . $filename;
	    else                                    
	      $file = $upload_dir['basedir'] . '/' . $filename;
	    file_put_contents($file, $image_data);

	    $wp_filetype = wp_check_filetype($filename, null );
	    $attachment = array(
	        'post_mime_type' => $wp_filetype['type'],
	        'post_title' => sanitize_file_name($filename),
	        'post_content' => '',
	        'post_status' => 'inherit'
	    );
	    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
	    require_once(ABSPATH . 'wp-admin/includes/image.php');
	    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
	    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
	    $res2= set_post_thumbnail( $post_id, $attach_id );
	}
	return $res2;
}

function CreatePostWP($title,$image,$content,$taxonomy,$post_type,$post_status = 'draft')
{
	 $content = disableLazyLoad($content);
	 $images = getImages($content);
	 $my_post = array(
	    'post_title'    => wp_strip_all_tags( $title ),
	    'post_content'  => $content,
	    'post_status'   => $post_status,
	    'post_type'   => $post_type,
	  );

	  $post_id = wp_insert_post( $my_post );
	  if($image != 'No' && $image != '')
	  {
	  	try {
	  		Generate_Featured_Image($image,$post_id);
	  	} catch (Exception $e){
	  		preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $img);
			if(isset($img['src']))
			{
			  Generate_Featured_Image($img['src'],$post_id);
			}
	  	}
	  	
	  }
	  
	  $taxonomy_args = array();
	  foreach($taxonomy as $item) {
		$term = get_term_by('name', $item, 'category');
		if($term) {
			array_push($taxonomy_args, $term);
		}
		
	  }
	  $result = wp_set_object_terms( $post_id, $taxonomy, 'category' );

	  return $post_id;
}
function RewriteArticles($id,$title,$content)
{
	
	
	$result = get_data_spin_by_id($id);
	$data = $result[0]->data_content;
	$textAr = explode("\n", $data);
	$textAr = array_filter($textAr, 'trim');

	if($data)
	{

		foreach ($textAr as $stringline) {
			//$recontent = $recontent.'<br>'.$stringline;
		    $string = str_replace('{','',$stringline);
		    $string = str_replace('}','',$string);
		    $array_string = explode('|', $string);
     		$content = StringtoSpinNormal($array_string,$content,$stringline);
		}
		foreach ($textAr as $stringline) {
			//$recontent = $recontent.'<br>'.$stringline;
		    $string = str_replace('{','',$stringline);
		    $string = str_replace('}','',$string);
		    $array_string = explode('|', $string);
     		$title = StringtoSpinNormal($array_string,$title,$stringline);
		}
		$content = stripslashes($content);
		$articles = array($title,$content);
		return $articles;

	}
	else
	{
		return false;
	}
}

function list_url_crawl($id) {


	$task = get_task_by_id($id);
	$task = json_decode($task);
	$urls = $task[0]->url_content;
	$list_urls = explode("\n", $urls);
	$url_selector = $task[0]->url_selector;
	
	$array_link = array();
	
	foreach ($list_urls as $url) {

		$data = ViewSource($url);
	  	$html = new simple_html_dom();
	  	$html->load($data);
		$result = $html->find($url_selector);
		$parse = parse_url($url);
		$domain = 'http://'.$parse['host'].'/';
		foreach($result as $element) 
		{
				//echo $element->href . '<br>';

				if(strpos($element->href, $parse['host']
					) !== false)
				{
					$domainfix = $element->href;
				}
				else
				{
					$domainfix = $domain.$element->href;
				}
		        array_push($array_link,$domainfix);
		}
	}
	
	$links = array_unique($array_link); 
	
	return $links;

	
}

function randomTask() {

	global $wpdb;
	$result = $wpdb->get_results( "SELECT id,title FROM {$wpdb->task_crawler} ORDER BY RAND()
		LIMIT 1");
	return json_encode($result);
	
}

function schedule_create_wp_post($id,$url) {

	$task = get_task_by_id($id);
	$task = json_decode($task);
	$titselect = $task[0]->title_selector;
	$artselect = $task[0]->content_selector;
	$imgselect = $task[0]->img_selector;
	$hasFeature = $task[0]->feature_image;
	$taxonomy =  $task[0]->taxonomy;
	$data_spin =  $task[0]->data_spin;
	$taxonomy_args = explode(',', $taxonomy);
	$post_type =  $task[0]->post_type;
	$post_status = 'draft';
	$data = ViewSource($url);
	$html = new simple_html_dom();
	$html->load($data);
	$article = $html->find($artselect);
	$title = $html->find($titselect);
	$text = strip_tags($title[0]);
	$content = $article[0];
	$img = '';
	if($imgselect != 'No' && $imgselect != '')
	{
		$images = $html->find($imgselect);
		foreach ($images as $image) {
			$img = $image->src;
		}
		
	}
	die();
	$text =  html_entity_decode($text);
// Insert the post into the database
	global $wpdb;
	$row = $wpdb->get_results( "SELECT * FROM {$wpdb->post_crawler} WHERE title = '{$text}' ");
	if(count($row) > 0)
	{
		echo 'Bài viết này đã có trong Database';
	}
	else
	{

		
		if($data_spin != 0)
		{
				$article = RewriteArticles($data_spin,$text,$content);
		}
		else
		{
				$article = false;
		}
		if($article != false)
		{
				
				$title = SpinArticle($article[0]);
				$content = SpinArticle($article[1]);
				$content = strip_tags($content,'<br><p><img><div>');
				$result2 = CreatePostWP($title,$img,$content,$taxonomy_args,$post_type,$post_status);
				if($result2)
				{
					echo 'Lấy tin thành công - '.$title;
					$result = wp_insert_autolinkpost($id,$text);
				}
				else
				{
					echo 'Lấy tin thất bại';
				}
			}
			else
			{
				$content = strip_tags($content,'<br><p><img><div>');
				$result2 = CreatePostWP($text,$img,$content,$taxonomy_args,$post_type,$post_status);
				if($result2)
				{
					echo 'Lấy tin thành công - '.$text;
					wp_insert_autolinkpost($id,$text);
				}
				else
				{
					echo $wpdb->last_error;
				}
		}

	}
}

function disableLazyLoad($content){

	$new_content = str_replace('src="http://toinayangi.vn/wp-content/plugins/lazy-load/images/1x1.trans.gif"', '', $content);
	$new_content = str_replace('data-lazy-src', 'src', $new_content);
	$new_content = str_replace('srcset', 'src-ex', $new_content);
	return $new_content;
}
?>