<?php 
/*
  Plugin Name: WP Auto Crawler
  Plugin URI: http://dinhvanvu.com/
  Description: Crawler Data in another website.
  Version: 1.0
  Author: Brian Samuel
  Author URI: http://dinhvanvu.com/
  Text Domain: WP Auto Crawler
*/
// Include Template 
include_once(plugin_dir_path( __FILE__ ).'template/generals/index.php' );
// Include Core
include_once(plugin_dir_path( __FILE__ ).'core/functions.php' );
include_once(plugin_dir_path( __FILE__ ).'core/tables.php' );
include_once(plugin_dir_path( __FILE__ ).'core/facecrawler.php' );
include_once(plugin_dir_path( __FILE__ ).'core/dataspin.php' );
include_once(plugin_dir_path( __FILE__ ).'core/task.php' );
include_once(plugin_dir_path( __FILE__ ).'core/post_logs.php' );
include_once(plugin_dir_path( __FILE__ ).'core/post_wp.php' );
include_once(plugin_dir_path( __FILE__ ).'core/schedule.php' );

include_once(plugin_dir_path( __FILE__ ).'core/ajax.php' );
register_activation_hook( __FILE__, 'wp_post_crawler_tables' );
register_activation_hook( __FILE__, 'wp_task_crawler_tables' );
register_activation_hook( __FILE__, 'wp_dataspin_crawler_tables' );
include(plugin_dir_path( __FILE__ ).'simple_html_dom.php' );
include(plugin_dir_path( __FILE__ ).'Spinner.php' );
function load_admin_css_wac()
{
    $plugin_url = plugin_dir_url(__FILE__);
    wp_enqueue_style('wac_bootstrap_min_css', $plugin_url.'/css/bootstrap.min.css');
    wp_enqueue_style('skin_icheck_css', $plugin_url.'/skins/square/blue.css');
    
    wp_enqueue_script('wac_bootstrap_min_js',$plugin_url.'/js/bootstrap.min.js', array('jquery'), true);
    //wp_enqueue_script('wac_notie_min_js',$plugin_url.'/js/notie.min.js', array('jquery'), true);
    wp_enqueue_script('jquery_table',$plugin_url.'/js/jquery.dataTables.min.js', array('jquery'), true);
    wp_enqueue_script('bootstrap_table',$plugin_url.'/js/dataTables.bootstrap.min.js', array('jquery'), true);
    if(isset($_GET['page']))
    {
      if($_GET['page'] == 'data-spin-page' || $_GET['page'] == 'wp-autocrawler' || $_GET['page'] == 'wac-logs-page' || $_GET['page'] == 'wac-setting-page')
      {
        wp_enqueue_script('icheck_js',$plugin_url.'/js/icheck.min.js', array('jquery'), true);
        wp_enqueue_script('wac_custom_js',$plugin_url.'/js/admin.js', array('jquery'), true);
      }
    }
    
    wp_localize_script('wac_custom_js', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));
}
add_action('admin_head','load_admin_css_wac');

?>