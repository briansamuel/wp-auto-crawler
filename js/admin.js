var $j = jQuery;
var custom_uploader_2;
var is_crawl = false;
var line = 0;
var lines = [];


function adddataspin(){

    var name = $j('#title-data').val();
    var content = $j('#keyword-data').val();

    $j.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            dataType: 'text',
            data: {action: 'ajax_insert_dataspin',name: name, content: content},
            success: function (data) {
                 alert(data);

                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
}
// End Add SPIN
function spinload(id){

    $j.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            dataType: 'text',
            data: {action: 'ajax_spin_load',id: id},
            success: function (data) {
                if(data != 'FailSpin')
                {
                    res = JSON.parse(data);
                    
                    name = res[0];
                    content = res[1];
                    
                    $j('#title-data').val(name);
                    $j('#keyword-data').val(content);
                    $j('#update-data-spin').prop('disabled', false);
                    $j('#delete-data-spin').prop('disabled', false);
                    $j('#add-data-spin').prop('disabled', true);
                }
                else
                {
                    alert('Không spin được');
                }
                //$j('#load-spin-animation').addClass("hidden-spin");
               //alert('ALO');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
}
// Jquery Get URL
function autogeturl(){

    var url = $j('#list_url_articles_test').val();
    var linkcss = $j('#a_selector').val();
    if(url == "" || linkcss == "")
    {
        alert('Vui lòng điền Url muốn lấy và Url CSS Selector');
    }
    else
    {
        $j('#list-link-auto-post').html('');
        
        $j.ajax({
                url: ajax_object.ajax_url,
                type: 'post',
                dataType: 'text',
                data: {action: 'ajax_get_url',url: url, linkcss: linkcss },
                success: function (data) {
                    
                    var result = JSON.parse(data);

                    for(var i in result)
                    {
                         var link = '<a href="'+result[i]+'">'+result[i]+'</a><br/>';
                         $j('#list-link-auto-post').append(link);
                    }
                    

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
    }
}
// End Get URL

// Jquery Get Articles
function autogetarticles(id){

    var url = $j('#url_articles_test').val();
    var titcss = $j('#title_selector').val();
    var artcss = $j('#content_selector').val();
    var exclude_html = $j('#exclude_html').val();
    if(url == '')
    {
        alert('Vui lòng nhập Url bài viết test thử');
    }
    else if(titcss == '')
    {
        alert('Vui lòng nhập Title Css Seletor');
    }
    else if(artcss == '')
    {
         alert('Vui lòng nhập Content Css Seletor');
    }
    else
    {
        $j('#load-spin-animation').removeClass("hidden-spin");
        $j('.title-post').html('');
        $j('.content-post').html('');
        $j.ajax({
                url: ajax_object.ajax_url,
                type: 'post',
                dataType: 'text',
                data: {action: 'ajax_get_post',url: url, titcss: titcss, artcss: artcss, exclude_html: exclude_html },
                success: function (data) {
                     $j('#load-spin-animation').addClass("hidden-spin");
                     //var result = JSON.parse(data);
                     var res = data.split("|-|-|");
                     $j('.title-post').html(res[1]);
                     $j('.content-post').html(res[0]);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
    }
}
// End Get Articles
// Jquery Save Task
function autosavetask(){

    var title = $j('#task-name').val();
    var taxonomy = [];
    $j('#taxonomy-list input:checked').each(function() {
        var selval = $j(this).val();
        taxonomy.push(selval);
        
    });

    var post_type = $j('#post-type-radio').find(":selected").val();
    var author = $j('#author').find(":selected").val();
    var publish_status = $j('#publish_status').find(":selected").val();
    var data_spin_articles = $j('#data_spin_articles').find(":selected").val();
    var list_url = $j('#urls').val();
    var url_selector = $j('#a_selector').val();
    var title_selector = $j('#title_selector').val();
    var content_selector = $j('#content_selector').val();
    var img_selector = $j('#image_selector').val();
    var token_task_edit = $j('#token_task_edit').val();
    var exclude_html = $j('#exclude_html').val();
    var json_type = $j('#json_type').find(":selected").val();
    var json_content = $j('#json_content').val();
    console.log('Token Task '+token_task_edit);
   
    if(title == '')
    {
        alert('Vui lòng thêm Title');
    }
    else if(list_url == '')
    {
        alert('Vui lòng thêm danh sách Url');
    }
    else if(url_selector == '')
    {
        alert('Vui lòng điền Url selector');
    }
    else if(title_selector == '')
    {
        alert('Vui lòng điền Title Selector');
    }
    else if(content_selector == '')
    {
        alert('Vui lòng điền Content Selector');
    }
    else if(img_selector == '')
    {
        img_selector = 'No';
    }
    else {        
        $j.ajax({
                url: ajax_object.ajax_url,
                type: 'post',
                dataType: 'text',
                data: { 
                    action: 'ajax_insert_task',
                    token_task_edit: token_task_edit,
                    title: title,
                    post_type: post_type,
                    taxonomy: taxonomy,
                    author: author,
                    publish_status: publish_status,
                    data_spin_articles: data_spin_articles,
                    list_url: list_url,
                    url_selector: url_selector,
                    title_selector: title_selector,
                    content_selector: content_selector,
                    img_selector: img_selector,
                    exclude_html: exclude_html,
                    json_type: json_type,
                    json_content: json_content,
                },
                success: function (data) {
                    alert(data);
                    

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
        });
    }
   
}
// End Save Task
// Jquery Get Task Ajax
function autogettask(id){


    $j.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            dataType: 'text',
            data: {action: 'ajax_get_list_post_by_task',id: id },
            success: function (data) {

                
                lines = JSON.parse(data);
                if(lines.length < 1) {
                    $j('.list-post-auto').append('Không lấy được dữ liệu, vui lòng cấu hình lại chiến dịch');
                } else {
                    autoinsertpost(id,lines[line]);
                }
                
                console.log(lines);


            },
            error: function (jqXHR, textStatus, errorThrown) {


                console.log(errorThrown);
            }
        }); 
}
// End Get Task Ajax
// Jquery Insert Post Ajax
function autoinsertpost(id,url){


    $j.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            dataType: 'text',
            data: {action: 'ajax_crawl_post',id: id,url: url },
            success: function (data) {

                var progress = (line+1)*100/lines.length;
                var total = objectLength(lines);
                $j('#progress-crawler').css('width',progress+'%');
                $j('#progress-crawler').attr('aria-valuenow', progress);
               
                $j('#progress-crawler').text((line + 1) + '/' + lines.length + ' đã xử lý ' + Math.round(progress) + '%');
                if(line < total-1)
                {
                    //console.log(lines[line]);
                    line = line + 1;
                    autoinsertpost(id,lines[line]);
                    
                } else {
                    line = 0;
                }
                
                $j('.list-post-auto').append(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var total = objectLength(lines);
                if(line < total-1)
                {
                    //console.log(lines[line]);
                    line = line + 1;
                    autoinsertpost(id,lines[line]);
                    
                }
                console.log(errorThrown);
            }
        }); 
}

function objectLength(obj) {
  var result = 0;
  for(var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
    // or Object.prototype.hasOwnProperty.call(obj, prop)
      result++;
    }
  }
  return result;
}
// End Insert Post Ajax
$j(document).ready(function ($) {

    //alert('ALert');
    $j('input').iCheck({
        checkboxClass: 'icheckbox_square-blue   ',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
    var t = $j('#table-task').DataTable({
        searching: true,
        paging: false,

    });
    var t = $j('#table-posts').DataTable({
        //"processing": true,
        "serverSide": true,
        "ajax": {
            "url": ajax_object.ajax_url,
            "type": "POST",
            "data": function ( d ) {
                d.action = 'datatable_post';
            }
        },
        "columns": [
            { "data": "id" },
            { "data": "id" },
            { "data": "title" },
            { "data": "url_post" },
            { "data": "post_status" },
            { "data": "post_date" },
        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox"  name="check-post[]" value="' + $('<div/>').text(data).html() + '">';
            }
         }],
         'order': [[1, 'asc']]

    });
    $j('#add-data-spin').click(function(){
        adddataspin();
    });
    $j('#test_get_list_articles').click(function(){


        autogeturl();
    });
    $j('#test_get_articles').click(function(){

        autogetarticles();
    });
    $j('#save-task').click(function(){

        autosavetask();
    });
    $j('.start-task').click(function(){

        var id = $j(this).attr('data-task');
        autogettask(id);
    });
    $j('#check-all').on('ifChecked', function(event){
        $j('input').iCheck('check');
    });
    $j('#check-all').on('ifUnchecked', function(event){
        $j('input').iCheck('uncheck');
    });
    $j('#data-spin-select-mng').on('change', function() {
      if(this.value != 0)
      {
        spinload(this.value);
        
      }
      else
      {
         $j('#title-data').val('');
         $j('#keyword-data').val('');
         $j('#add-data-spin').prop('disabled', false);
         $j('#update-data-spin').prop('disabled', true);
         $j('#delete-data-spin').prop('disabled', true);
       // or $j(this).val()
      }
    });
    $j('#update-data-spin').click(function(){
      var id = $j( "#data-spin-select-mng option:selected" ).val();
      spinupdate(id);

    });
    $j('#delete-data-spin').click(function(){
      var id = $j( "#data-spin-select-mng option:selected" ).val();
      spindelete(id);

    });
    $j('#CrawlPage').click(function(){
        
        lines = $j('#list-crawl').val().split('\n');
        //console.log(lines.length);
        crawl_anime_2(lines[line]);
    });
    
});